package com.power.doc;

/**
 * api生成进度监控器
 * @author lianyl
 *
 */
public interface IApiBuildMonitor {
	void process(double percent);
}
