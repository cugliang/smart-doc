package com.power.doc;

import com.power.doc.model.ApiDoc;

/**
 * api文档处理器
 * @author lianyl
 *
 */
public interface IApiDocProcessor {
	/**
	 * 处理api文档
	 * @param apiDoc
	 */
	void process(ApiDoc apiDoc);
}
