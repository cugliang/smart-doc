package com.power.doc;

import com.thoughtworks.qdox.model.JavaClass;

/**
 * 工程文档配置创建器
 * @author lianyl
 *
 */
public interface IProjectDocConfigBuilder {
	/**
	 * 根据类名称获取类对象
	 * @param simpleName
	 * @return
	 */
	JavaClass getClassByName(String simpleName);
}
