package com.power.doc;

public interface IApiUrlParser {
	String parse(String clazName,String url);
}
